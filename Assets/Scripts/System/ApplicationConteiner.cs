﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationConteiner : Singleton<ApplicationConteiner>
{
  [SerializeField]
  private GameFildController _gameFildController;
  [SerializeField]
  private EventHollder _eventHolder;
  [SerializeField]
  private UIController _uiController;

  private bool _isObjectTarget = false;

  public bool IsObjectTarget
  {
    get { return _isObjectTarget; }
    set { _isObjectTarget = value; }
  }

  public GameFildController GameFildController
  {
    get { return _gameFildController; }
  }

  public EventHollder EventHollder
  {
    get { return _eventHolder; }
  }

  public UIController UIController
  {
    get { return _uiController; }
  }


  void Start ()
  {
    InitializeObjects();

  }

  private void InitializeObjects()
  {
    _gameFildController.Initializate();
  }
}
