﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventHollder : MonoBehaviour
{
  public Action<int> OnItemShopClickEvent;
  public void OnItemShopClick(int id)
  {
    if(OnItemShopClickEvent != null)
    {
      OnItemShopClickEvent(id);
    }
  }

  public Action<Vector2> OnObjectSetEvent;
  public void OnObjectSet(Vector2 pos)
  {
    if(OnItemShopClickEvent != null)
    {
      OnObjectSetEvent(pos);
    }
  }

}
