﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour
{
  [SerializeField]
  private SpriteRenderer _spriteRenderer;
  [SerializeField]
  private Sprite _borderEmpty;
  [SerializeField]
  private Sprite _borderNotEmpty;
  [SerializeField]
  private Vector2 _posInGread;

  private List<Vector2> _neighbors;
  private bool _isEmpty = true;

  public void Initializate(int x, int y)
  {
    _neighbors = new List<Vector2>();
    _posInGread = new Vector2(x, y);
    AddNeighbors((int)_posInGread.x, (int)_posInGread.y);
  }


  public bool IsEmpty
  {
    get { return _isEmpty; }
  }

  public List<Vector2> neighbors
  {
    get { return _neighbors; }
  }

  public void ShowHideBorder(bool state)
  {
    _spriteRenderer.enabled = state;
  }

  public void SetNotEmptyCell()
  {

    if (_isEmpty)
    {
      _spriteRenderer.sprite = _borderNotEmpty;
      _isEmpty = false;
    }
  }

  public void OnMouseEnter()
  {
    if (ApplicationConteiner.Instance.IsObjectTarget)
    {
      ApplicationConteiner.Instance.GameFildController.TempObject.transform.position = transform.position;
    }
    Debug.Log("Enter");
  }

  public void OnMouseUp()
  {
    if (ApplicationConteiner.Instance.IsObjectTarget)
    {
      ApplicationConteiner.Instance.EventHollder.OnObjectSet(_posInGread);
      ApplicationConteiner.Instance.IsObjectTarget = false;
    }
  }

  public void OnMouseExit()
  {
    Debug.Log("Exit");
  }

  public void AddNeighbors(int x, int y)
  {
    if (x - 1 >= 0)
    {
      _neighbors.Add(new Vector2(x - 1, y));
    }
    if (x - 1 >= 0 && y - 1 >= 0)
    {
      _neighbors.Add(new Vector2(x - 1, y - 1));
    }
    if (y - 1 >= 0)
    {
      _neighbors.Add(new Vector2(x, y - 1));
    }
    if (x + 1 < ApplicationConteiner.Instance.GameFildController.Width && y - 1 >= 0)
    {
      _neighbors.Add(new Vector2(x + 1, y - 1));
    }
    if (x + 1 < ApplicationConteiner.Instance.GameFildController.Width)
    {
      _neighbors.Add(new Vector2(x + 1, y));
    }
    if (x + 1 < ApplicationConteiner.Instance.GameFildController.Width && y + 1 < ApplicationConteiner.Instance.GameFildController.Width)
    {
      _neighbors.Add(new Vector2(x + 1, y + 1));
    }
    if (y + 1 < ApplicationConteiner.Instance.GameFildController.Width)
    {
      _neighbors.Add(new Vector2(x, y + 1));
    }
    if (x - 1 >= 0 && y + 1 < ApplicationConteiner.Instance.GameFildController.Width)
    {
      _neighbors.Add(new Vector2(x - 1, y + 1));
    }
  }

  public void SetNotEmptyCellNeighbors()
  {
    foreach (Vector2 neighbor in _neighbors)
    {
      ApplicationConteiner.Instance.GameFildController.Cells[(int)neighbor.x, (int)neighbor.y].SetNotEmptyCell();
    }
  }
}
