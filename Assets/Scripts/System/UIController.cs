﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{

  [SerializeField]
  private Shop _shop;

  private bool _isActivGread = true;
  private bool _isActivShop = false;

  public void OnClickGredButton()
  {
    ApplicationConteiner.Instance.GameFildController.ShowHideGread(!_isActivGread);
    _isActivGread = !_isActivGread;
  }

  public void OnShopClick()
  {
    _shop.gameObject.SetActive(!_isActivShop);
    _shop.Initializate();
    _isActivShop = !_isActivShop;
  }

}
