﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
  [SerializeField]
  private Sprite _icon;

  private int _id;
  private Vector2 _pos;
  private string _name;

  public Sprite Icon
  {
    get { return _icon; }
  }

	public void Initializate(int id, Vector2 pos, string name)
  {
    _id = id;
    _pos = pos;
    _name = name;
  }

  public void SetPos(Vector2 pos)
  {
    _pos = pos;
  }

  private void OnMouseUp()
  {
    Debug.Log(string.Format("ID = {0}, Pos X = {1}, Pos Y = {2}, Name = {3}", _id, _pos.x, _pos.y, _name));
  }
}
