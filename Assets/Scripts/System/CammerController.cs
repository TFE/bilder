﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CammerController : MonoBehaviour
{

  private float _spead = 0.01f;
  private Vector2 _f0start = Vector2.zero;
  Vector2 _f1start = Vector2.zero;

  void Update()
  {
    if (!EventSystem.current.IsPointerOverGameObject())
    {
#if UNITY_EDITOR
      Mouse();
#else
      //;
      Touh();
#endif
    }


  }
  void Mouse()
  {

    if (Input.GetMouseButton(0))
    {

      Camera.main.transform.position += new Vector3(Input.GetAxis("Mouse X"), 0, Input.GetAxis("Mouse Y"));
      
    }
    Camera.main.transform.position += new Vector3(0, Input.GetAxis("Mouse ScrollWheel"), 0);
  }
  void Touh()
  {

    if(Input.touchCount == 1)
    {
      Camera.main.transform.position += new Vector3(Input.GetTouch(0).deltaPosition.x * _spead, 0, Input.GetTouch(0).deltaPosition.y * _spead);
      _f0start = Vector2.zero;
      _f1start = Vector2.zero;
    }

    if(Input.touchCount > 1)
    {
      Zoom();
    }
  }

  void Zoom()
  {
    if (_f0start == Vector2.zero && _f1start == Vector2.zero)
    {
      _f0start = Input.GetTouch(0).position;
      _f1start = Input.GetTouch(1).position;
    }

    Vector2 f0position = Input.GetTouch(0).position;
    Vector2 f1position = Input.GetTouch(1).position;

    float dir = Mathf.Sign(Vector2.Distance(_f1start, _f0start) - Vector2.Distance(f0position, f1position));

    Camera.main.transform.position = Vector3.MoveTowards(transform.position, transform.position + transform.forward, dir * _spead * Time.deltaTime * Vector3.Distance(f0position, f1position));
  }
}
