﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameFildController : MonoBehaviour
{
  [SerializeField]
  private GameObject _cellPrefab;
  [SerializeField]
  private Vector3 _startPos;

  [SerializeField]
  GameObject[] _decoration;

  private List<GameObject> _objectsInScene;

  private GameObject _tempObject; 

  private Cell[,] _cells;
  private float _step =1f;

  private const int WIDTH = 10;

  public GameObject[] Decoration
  {
    get { return _decoration; }
  }

  public int Width
  {
    get { return WIDTH; }
  }

  public Cell[,] Cells
  {
    get { return _cells; }
  }

  public GameObject TempObject
  {
    get { return _tempObject; }
  }

  public void Initializate()
  {
    ApplicationConteiner.Instance.EventHollder.OnItemShopClickEvent += CreteObgect;
    ApplicationConteiner.Instance.EventHollder.OnObjectSetEvent += SetObject;

    _objectsInScene = new List<GameObject>();

    CreateGred();
    CreteObgect(1, 2,3);
    CreteObgect(0, 5, 6);
  }

  private void CreateGred()
  {
    _cells = new Cell[WIDTH,WIDTH];
    
    for(int i = 0; i < WIDTH; ++i)
    {
      Vector3 pos = _startPos;
      for (int j = 0; j < WIDTH; ++j)
      {
        _cells[i, j] = Instantiate(_cellPrefab, transform).GetComponent<Cell>();
        _cells[i, j].transform.position = pos;
        _cells[i, j].Initializate(i, j);
        pos = new Vector3(pos.x + _step, pos.y, pos.z);
      }
      _startPos = new Vector3(_startPos.x, _startPos.y, _startPos.z - _step);
    }
  }

  public void ShowHideGread(bool state)
  {
    for(int i = 0; i < WIDTH; ++i)
    {
      for(int j = 0; j < WIDTH; ++j)
      {
        _cells[i, j].ShowHideBorder(state);
      }
    }
  }

  public void CreteObgect(int indexObject, int x, int y)
  {
    GameObject tempObgect = Instantiate(_decoration[indexObject], gameObject.transform);
    tempObgect.transform.position = _cells[x, y].transform.position;
    tempObgect.GetComponent<Item>().Initializate(indexObject, new Vector2(x, y), tempObgect.name);
    _objectsInScene.Add(tempObgect);
    _cells[x, y].SetNotEmptyCell();
    SetNotEmptyCellNeighbors(_cells[x, y].neighbors);
  }

  public void SetNotEmptyCellNeighbors(List<Vector2> neighbors)
  {
    foreach (Vector2 neighbor in neighbors)
    {
      _cells[(int)neighbor.x, (int)neighbor.y].SetNotEmptyCell();
    }
  }

  private void CreteObgect(int id)
  {
    ApplicationConteiner.Instance.IsObjectTarget = true;
    _tempObject = Instantiate(_decoration[id], transform);
    _tempObject.GetComponent<BoxCollider>().enabled = false;
    _tempObject.GetComponent<Item>().Initializate(id, new Vector2(0, 0), _tempObject.name);
    ActiveDeactiveObjects(false);
  }

  private void SetObject(Vector2 pos)
  {
    if (CheckPos(pos))
    {
      _tempObject.transform.position = _cells[(int)pos.x, (int)pos.y].transform.position;
      _tempObject.GetComponent<Item>().SetPos(pos);
      _objectsInScene.Add(_tempObject);
      _cells[(int)pos.x, (int)pos.y].SetNotEmptyCell();
      SetNotEmptyCellNeighbors(_cells[(int)pos.x, (int)pos.y].neighbors);
    }
    else
    {
      DestroyObject(_tempObject);
    }
    ActiveDeactiveObjects(true);
  }

  private bool CheckPos(Vector2 pos)
  {
    foreach (Vector2 item in _cells[(int)pos.x, (int)pos.y].neighbors)
    {
      if(!_cells[(int)item.x, (int)item.y].IsEmpty)
      {
        return false;
      }
    }
    return true;
  }

  private void ActiveDeactiveObjects(bool state)
  {
    foreach (GameObject item in _objectsInScene)
    {
      item.GetComponent<BoxCollider>().enabled = state;
    }
  }
}
