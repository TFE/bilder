﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopItem : MonoBehaviour
{
  [SerializeField]
  private Button _buton;
  [SerializeField]
  Text _lableHolder;
  [SerializeField]
  private Image _icon;

  public Button Button
  {
    get { return _buton; }
  }

  public void Initializate(string lable, Sprite icon)
  {
    _lableHolder.text = lable;
    _icon.sprite = icon;

  }
}
