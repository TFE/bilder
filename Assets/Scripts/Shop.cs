﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
  [SerializeField]
  Transform _parent;
  [SerializeField]
  private Button _itemPrefab;

  private List<ShopItem> _items;
	public void Initializate()
  {
    if(_items == null)
    {
      _items = new List<ShopItem>();
      for(int i = 0; i < ApplicationConteiner.Instance.GameFildController.Decoration.Length; ++i)
      {
        ShopItem tempBut = Instantiate(_itemPrefab, _parent).GetComponent<ShopItem>();
        tempBut.Initializate(ApplicationConteiner.Instance.GameFildController.Decoration[i].name, ApplicationConteiner.Instance.GameFildController.Decoration[i].GetComponent<Item>().Icon);
        int id = i;
        tempBut.Button.onClick.AddListener(()=> { OnClickItem(id); });
        _items.Add(tempBut);
      }
    }
  }

  private void OnClickItem(int id)
  {
    ApplicationConteiner.Instance.UIController.OnShopClick();
    ApplicationConteiner.Instance.EventHollder.OnItemShopClick(id);
  }
}
